# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django import template
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.shortcuts import render

from .models import Profile, Location, Camera
from django.contrib.auth.models import User


@login_required(login_url="/login/")
def index(request):
    context = {'segment': 'index'}

    if request.method == 'GET':
        user_id = request.user.id
        context['camera'] = Camera.objects.filter(location__profile__user_id=user_id)

    html_template = loader.get_template('home/index.html')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]

        if load_template == 'admin':
            return HttpResponseRedirect(reverse('admin:index'))
        context['segment'] = load_template

        html_template = loader.get_template('home/' + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('home/page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:
        html_template = loader.get_template('home/page-500.html')
        return HttpResponse(html_template.render(context, request))


def profile(request):
    if request.method == 'POST':
        print(request.POST)
        pass
    return render(request, 'home/ui-forms2.html')

def update_profile(request):
    if request.method == 'POST':
        d = request.POST
        print(d)
        user_form = User.objects.get(id=request.user.id)
        profile_form = Profile.objects.get(id=request.user.id)
        for key, value in d.items():
            try:
                if key in vars(user_form):
                    setattr(user_form, key, value)
                elif key in vars(profile_form):
                    setattr(profile_form, key, value)
            except Exception as e:
                print(e)     
        user_form.save()
        profile_form.save()
    return render(request, 'home/ui-forms2.html')