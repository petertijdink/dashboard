from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

import uuid


class Location(models.Model):

    street = models.CharField(max_length=30, blank=True)
    housenumber = models.CharField(max_length=30, blank=True)
    postal_code = models.CharField(max_length=30, blank=True)
    city = models.CharField(max_length=30, blank=True)
    country = models.CharField(max_length=30, blank=True)

    def __str__(self):
        name = f"{self.city} - {self.street} {self.housenumber}"
        return name


class Profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.ManyToManyField(Location, blank=True)
    
    telefoon_nummer = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.user.username

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class Trap(models.Model):
    
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    unique_uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Camera(models.Model):
    
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    unique_uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    status = models.BooleanField(default=False, editable=False)